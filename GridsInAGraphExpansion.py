#Grids in a graph 
#Learning path planning 
#Donald Posterick

from graphics import *
from time import *
from Node import *
import queue




GridSize = [10,10]#10 width, 20 tall
WindowSize = [800,800]#x,y
WindowCoords = [GridSize[0],GridSize[1]]
WindowDeadzone = 2
NodeXSpacing = (WindowCoords[0]) / GridSize[0]
NodeYSpacing = (WindowCoords[1]) / GridSize[1]
#A node is a single [x,y] point on the grid

diagMovementEnabled = False
ObstacleLocations = [[1,1],[2,3],[4,7],[4,6],[8,4],[4,8],[13,7],[3,9]]

start = Node(0,0)
end = Node(9,9)

all_nodes = []
frontier = queue.deque()
explored_nodes = []

frontier.append(start.getCoords())

for x in range(GridSize[0]):
    for y in range(GridSize[1]):
        all_nodes.append(Node(x,y))

#print("All nodes")
#print(all_nodes)

def main():
    win = GraphWin("Grid Graph", WindowSize[0],WindowSize[1])

    win.setCoords(0,0,WindowCoords[0],WindowCoords[1])

    drawAllNodes(win)

    drawNode(win,start,"red")
    drawNode(win,end,"purple")

    for node in all_nodes:
        #sleep(.01)
        print(node.getCoords())
        drawNeighborLines(win, node)

    for StartingNeighbor in getNeighbors(start):
        frontier.append(StartingNeighbor)

    frontier.remove(start.getCoords())
    explored_nodes.append(start.getCoords())

    while end.getCoords() not in explored_nodes:
        current = frontier.popleft()
        explored_nodes.append(current)
        for node in getNeighbors(Node(current[0],current[1])):
            frontier.append(node)	
        print(frontier)	
        for node in explored_nodes:
            drawNode(win, node, "green")
        for node in frontier:
            drawNode(win, node, "purple")


    print("Found the end")
    
    win.getMouse()
    win.close()


def drawNode(_win, node, color):
    NodePoint = getNodePoint(node)
    nodeSpacingMin = NodeYSpacing
    if NodeXSpacing < NodeYSpacing:
        nodeSpacingMin = NodeXSpacing
    nodeCircle = Circle(NodePoint, (nodeSpacingMin)/3.14)
        
    nodeCircle.setFill(color)
    nodeCircle.draw(_win)

def drawAllNodes(_win):
    for node in all_nodes:
        drawNode(_win, node, "grey")

def getNodePoint(node):
    if type(node) == type(Node()):
        return Point(NodeXSpacing*node.getX()+NodeXSpacing/2, NodeYSpacing*node.getY()+NodeYSpacing/2)
    else:
        return Point(NodeXSpacing*node[0]+NodeXSpacing/2, NodeYSpacing*node[1]+NodeYSpacing/2) 

def drawNeighborLines(_win, node):
    neighbors = getNeighbors(node)
    for nodeN in neighbors:
        NodePoint = getNodePoint(node)
        NeighborNode = getNodePoint(nodeN)
        NeighborLine = Line(NodePoint, NeighborNode)
        NeighborLine.setWidth(4)
        NeighborLine.draw(_win)
    pass

def getNeighbors(_node, diagMovement = diagMovementEnabled):
    if diagMovement:#E, NE, N, NW, W, SW, S, SE
        dirs = [[1,0], [1,1], [0,1],[-1,1],[-1,0],[-1,-1],[0,-1],[1,-1]] 
    else:#east, north, west, south 
        dirs = [[1,0], [0,1],[-1,0],[0,-1]] 

    neighboringNodes = []

    if _node.getCoords() in ObstacleLocations:
        return neighboringNodes

    for dir in dirs:
        neighbor = [_node.getX() + dir[0], _node.getY() + dir[1]]
        for node in all_nodes: #only get neighbors within bounds
            if node.getX() == neighbor[0] and node.getY() == neighbor[1]:
                if neighbor in ObstacleLocations:
                    node.setWeight(10)
                    print("Obstacle detected at " + str(node.getCoords()))
                    
                else:
                    if neighbor not in frontier and neighbor not in explored_nodes:
                        neighboringNodes.append(neighbor)

            
    return neighboringNodes

if __name__ == "__main__":
    main()

