#Grids in a graph 
#Learning path planning 
#Donald Posterick

from graphics import *
from time import *
from Node import *
import queue
import random

delay = .001


GridSize = [10,10]#10 width, 20 tall
WindowSize = [800,800]#x,y
WindowCoords = [GridSize[0],GridSize[1]]
WindowDeadzone = 2
NodeXSpacing = (WindowCoords[0]) / GridSize[0]
NodeYSpacing = (WindowCoords[1]) / GridSize[1]
#A node is a single [x,y] point on the grid

diagMovementEnabled = True

obstacleCount = 5
obstacles = []

for i in range(obstacleCount):
    obstacles.append([random.randint(0,GridSize[0]),random.randint(0,GridSize[1])])

ObstacleLocations = []

all_nodes = []
frontier = queue.deque()
explored_nodes = []



#print("All nodes")
#print(all_nodes)


def main():
    
    for x in range(GridSize[0]):
        for y in range(GridSize[1]):
            all_nodes.append(Node(x,y))

    start = getNode(0,0)#Node(0,0)
    start.setParentNode(None)
    end = getNode(9,9)#Node(9,9)

    for obby in obstacles:
        if getNode(obby[0], obby[1]) == start or getNode(obby[0], obby[1]) == end:
            continue
        ObstacleLocations.append(getNode(obby[0], obby[1]))        

    frontier.append(start)



    win = GraphWin("Grid Graph", WindowSize[0],WindowSize[1])

    win.setCoords(0,0,WindowCoords[0],WindowCoords[1])

    drawAllNodes(win)

    drawNode(win,start,"blue")
    drawNode(win,end,"orange")

    for node in all_nodes:
        #sleep(.01)
        #print(node.getCoords())
        drawNeighborLines(win, node)

    #for StartingNeighbor in getNeighbors(start):
    #    StartingNeighbor.setParentNode(start)
    #    frontier.append(StartingNeighbor)

    #frontier.remove(start)
    #explored_nodes.append(start)

    while end not in explored_nodes:
        #sleep(delay)

        current = None
        try:
            current = frontier.popleft()
        except IndexError as e:
            print("No solution found")
            text = Text(Point(WindowCoords[0]/2,WindowCoords[1]/2),"No solution found")
            text.setSize(35)
            text.setTextColor("red")
            text.setStyle("bold")
            text.draw(win)
            win.getMouse()
            exit(-1)
        
        drawNode(win, current, "green")
        explored_nodes.append(current)
        for node in getNeighbors(current):
            node.setParentNode(current)
            if node == end:
                print("End node reached: " + str(node))
            frontier.append(node)	
        #print(frontier)	
        #for node in explored_nodes:
        #    drawNode(win, node, "green")
        for node in frontier:
            drawNode(win, node, "purple")
        drawNode(win,start,"blue")


    print("Found the end")
    path = []
    node = end
    while not (node.getParentNode() == None):
        path.append(node)
        node = node.getParentNode()
        
    path.reverse()

    for pathNode in path:
        drawNode(win,pathNode,"red")
        sleep(.1)
    drawNode(win,start,"red")
    #print("Path: " + str(path))
    
    win.getMouse()
    win.close()

def getNode(x,y):
    if x >= GridSize[0] or x < 0 or y >= GridSize[1] or y < 0:
        return None
    return all_nodes[GridSize[0]*x + y]


def drawNode(_win, node, color):
    NodePoint = getNodePoint(node)
    nodeSpacingMin = NodeYSpacing
    if NodeXSpacing < NodeYSpacing:
        nodeSpacingMin = NodeXSpacing
    nodeCircle = Circle(NodePoint, (nodeSpacingMin)/3.14)
        
    nodeCircle.setFill(color)
    nodeCircle.draw(_win)

def drawAllNodes(_win):
    for node in all_nodes:
        drawNode(_win, node, "grey")

def getNodePoint(node):
    if type(node) == type(Node()):
        return Point(NodeXSpacing*node.getX()+NodeXSpacing/2, NodeYSpacing*node.getY()+NodeYSpacing/2)
    else:
        return Point(NodeXSpacing*node[0]+NodeXSpacing/2, NodeYSpacing*node[1]+NodeYSpacing/2) 

def drawNeighborLines(_win, node):
    neighbors = getNeighbors(node)
    for nodeN in neighbors:
        NodePoint = getNodePoint(node)
        NeighborNode = getNodePoint(nodeN)
        NeighborLine = Line(NodePoint, NeighborNode)
        NeighborLine.setWidth(4)
        NeighborLine.draw(_win)

def getNeighbors(_node, diagMovement = diagMovementEnabled):
    if diagMovement:#E, NE, N, NW, W, SW, S, SE
        dirs = [[1,0], [1,1], [0,1],[-1,1],[-1,0],[-1,-1],[0,-1],[1,-1]] 
    else:#east, north, west, south 
        dirs = [[1,0], [0,1],[-1,0],[0,-1]] 

    neighboringNodes = []

    if _node in ObstacleLocations:
        return neighboringNodes

    for dir in dirs:
        neighbor = getNode(_node.getX() + dir[0], _node.getY() + dir[1])
        for node in all_nodes: #only get neighbors within bounds
            if node == neighbor:
                if neighbor in ObstacleLocations:
                    node.setWeight(10)
                    #print("Obstacle detected at " + str(node.getCoords()))
                    continue
                else:
                    if neighbor not in frontier and neighbor not in explored_nodes:
                        neighboringNodes.append(neighbor)

            
    return neighboringNodes

if __name__ == "__main__":
    main()

