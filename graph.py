from Node import Node
from graphics import *
import math
import queue
import random

class Graph:

    def __init__(self, _widthNode, _heightNode, generateWindow = True, windowWidth = 800, windowHeight = 800):
        self.all_nodes = []
        self.width = _widthNode
        self.height = _heightNode
        self.ObstacleNodes = []
        self.FrontierList = []
        self.Frontier = queue.Queue()
        self.highLightedNodes = {}
        self.generateWindow = generateWindow
        self.ObstacleColor = "red"
        self.DefaultNodeColor = "white"
        self.VisitedNodeColor = "grey"
        self.FrontierColor = "green"

        for x in range(self.width):
            for y in range(self.height):
                self.all_nodes.append(Node(x,y))

        if self.generateWindow == True:
            self.win = GraphWin("Graph",windowWidth,windowHeight)
            self.win.setCoords(0,0,self.width,self.height)

    def createRandomObstacles(self, obbyCount, ignoreNodes=[]):
        for obby in range(obbyCount):
            x = random.randint(0,self.width)
            y = random.randint(0,self.height)
            if self.getNode(x, y) in ignoreNodes:
                continue
            self.addObstacle(x,y)   

    def addObstacle(self, x, y):
        node = self.getNode(x,y)
        if not type(node) == type(None):
            self.ObstacleNodes.append(node)

    def getNode(self,x,y):
        if x >= self.width or x < 0 or y >= self.height or y < 0:
            return None
        return self.all_nodes[self.width*x + y]

    def addToFrontier(self, _node):
        self.Frontier.put(_node)
        self.FrontierList.append(_node)

    def getFromFrontier(self):
        try:
            node = self.Frontier.get(False)
            self.FrontierList.remove(node)
            return node
        except:
            return None

    def isFrontierEmpty(self):
        return not self.Frontier.not_empty

    def drawTiles(self):
        for node in self.all_nodes:
            rect = Rectangle(Point(node.getX(), node.getY()), Point(node.getX()+1, node.getY()+1))
            if node in self.ObstacleNodes:
                rect.setFill(self.ObstacleColor)
            elif node in self.highLightedNodes:
                rect.setFill(self.highLightedNodes.get(node))
            elif node in self.FrontierList:
                rect.setFill(self.FrontierColor)
            elif node.isVisited():
                rect.setFill(self.VisitedNodeColor)
            else:
                rect.setFill(self.DefaultNodeColor)
            rect.draw(self.win)
            
    def highlightNode(self, _node, color):
        self.highLightedNodes[_node] = color

    def clearHighlightedNodes(self):
        self.highLightedNodes = {}

         
    def getMouse(self):
        return self.win.getMouse()

    def getClickedNode(self):
        clickedPoint = self.win.getMouse()
        return self.getNode(math.floor(clickedPoint.getX()), math.floor(clickedPoint.getY()))

    def displayNoSolution(self):
        print("No solution found")
        text = Text(Point(self.width/2,self.height/2),"No solution found")
        text.setSize(35)
        text.setTextColor("dark green")
        text.setStyle("bold")
        
        for i in range(30):
            try:
                text.setTextColor(str(color_rgb(random.randint(0,255),random.randint(0,255),random.randint(0,255))))
                text.draw(self.win)
                time.sleep(1)
            except:
                time.sleep(1)

    def getObstacleCoords(self):
        obstacles = []
        for obbyNode in self.ObstacleNodes:
            obstacles.append(obbyNode.getCoords())  
        return obstacles   
        
    def setObstacleCoords(self, obstacleCoords):
        self.ObstacleNodes = []
        for nodeCoords in obstacleCoords:
            try:
                node = self.getNode(nodeCoords.getX(), nodeCoords.getY()) 
                if not type(node) == type(None):
                    self.ObstacleNodes.append(node)
            except:
                pass

    
    def updateWindow(self):
        self.win.redraw()

    def getNeighbors(self, _node, diagMovement = False):
        if diagMovement:#E, NE, N, NW, W, SW, S, SE
            dirs = [[1,0], [1,1], [0,1],[-1,1],[-1,0],[-1,-1],[0,-1],[1,-1]] 
        else:#east, north, west, south 
            dirs = [[1,0], [0,1],[-1,0],[0,-1]] 

        neighboringNodes = []

        if _node in self.ObstacleNodes:
            return neighboringNodes

        for dir in dirs:
            neighbor = self.getNode(_node.getX() + dir[0], _node.getY() + dir[1])
            for node in self.all_nodes: #only get neighbors within bounds
                if node == neighbor:
                    neighboringNodes.append(neighbor)
                
        return neighboringNodes


if __name__ == "__main__":
    graph = Graph(10,10)
    graph.getNode(2,4).setVisited()
    graph.createRandomObstacles(10)
    graph.drawTiles()
    graph.getClickedNode().setVisited()
    graph.highlightNode(graph.getNode(0,2),"orange")
    graph.drawTiles()
    print(graph.getClickedNode().getCoords())