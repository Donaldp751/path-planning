import json

class mapLoader:
	def __init__(self, fileName):
		self.fileName = fileName

	def open(self): 
		try:
			self.fileHandle = open(self.fileName, "rw+", None, "ascii")
		except:
			return False
		return True

	def close(self):
		try:
			self.fileHandle.close()
		except:
			pass
			
	def saveMap(self, mapCoords):
		if len(mapCoords) > 0:
			try:
				jsonString = json.dumps(mapCoords, indent=4)
				self.fileHandle.write(jsonString)
				return True
			except:
				return False	
		else:
			return False