#Name: AStar path-planning algorithm visual demonstration
#Author: Donald Posterick
#Date: 4/30/2019

from graph import *
from Node import *

NodesToGenerateInMap = 5000 #How many nodes to generate in the map
RandomObstacleMaxCount = 1700 #Max number of obstacles to generate in the map
EnableDiagionalMovement = False #Are diagional movements through the map allowed? False = N,S,E,W : True = N,NW,W,SW,S,SE,E,NE 
EnableClosedNodesHighlighting = True #Show closed nodes in map
EnableOpenNodesHighlighting = True #Show the current open nodes

#higher heuristicWeight value means more weight on the manhattan distance to the destination.
heuristicWeight = 2 #lower means the search funnels out searching out for more optimal paths

ClosedNodesColor = "grey"
OpenNodesColor = "green"

NodeX = round(math.sqrt(NodesToGenerateInMap))
NodeY = NodeX

graph = Graph(NodeX,NodeY,True, 800, 800, ) #NodesWidth, NodeHeight, WindowPixelWidth, WindowPixelHeight
graph.createRandomObstacles(RandomObstacleMaxCount) #Max number of obstacles to generate in the map

obbyCount = 0 #For clicking obstacles into the map

for i in range(obbyCount):
	if i % 10 == 0:
		graph.drawTiles()
	clickedNode = graph.getClickedNode()
	graph.addObstacle(clickedNode.getX(), clickedNode.getY())
	print("Obstacle added at: " + str(clickedNode.getCoords()) + " obstacles left: " + str(obbyCount - i - 1))

graph.drawTiles()

print("Click in starting node")

start = graph.getClickedNode()

print("Starting point = " + str(start.getCoords()))
graph.highlightNode(start, "purple")

print("Click in destination node")

end = graph.getClickedNode()
print("Destination point = " + str(end.getCoords()))

graph.highlightNode(end, "teal")


openNodes = []
closedNodes = []

openNodes.append(start)

def getNodeCost(node, destNode):
	return g(node) + h(node, destNode)


def g(node):#operating cost function - Actual operating cost having been already traversed
	cost = 1
	traversedNodes = []
	while not node == start:
		try:
			if node in traversedNodes:
				return float("inf")
			traversedNodes.append(node)
			node = node.getParentNode()
			cost = cost+1
		except:
			return cost
		
	return cost

def h(node, destNode): #heuristic function for calculating the distance between the node and the destination node
	#"""Returns the distance between the node""""
	destinationCoords = destNode.getCoords()
	nodeCoords = node.getCoords()
	xDiff = abs(destinationCoords[0]-nodeCoords[0])
	yDiff = abs(destinationCoords[1]-nodeCoords[1])
	return math.sqrt(xDiff*xDiff + yDiff*yDiff) * heuristicWeight

def getLowestNode():
	lowest = float("inf")
	lowestCostingNode = Node(0,0)
	for node in openNodes:
		nodeCost = getNodeCost(node, end)
		if nodeCost < lowest:
			lowest = nodeCost
			lowestCostingNode = node
	return lowestCostingNode


def AStar():
	#graph.getNode(4,3).setParentNode(graph.getNode(3,3))
	#print(g(graph.getNode(4,3)))
	#print(h(graph.getNode(4,3), graph.getNode(8,8)))
	path = []
	while len(openNodes):
		currentBestNode = getLowestNode()
		openNodes.remove(currentBestNode)
		closedNodes.append(currentBestNode)

		if EnableOpenNodesHighlighting:
			for node in openNodes:
				graph.highlightNode(node, OpenNodesColor)
		if EnableClosedNodesHighlighting:
			for node in closedNodes:
				graph.highlightNode(node, ClosedNodesColor)

		if currentBestNode == end:
			print("goal met")
			while not currentBestNode == start:
				path.append(currentBestNode)
				try:
					currentBestNode = currentBestNode.getParentNode()
				except:
					path.reverse()
			break
		else:
			neighbors = graph.getNeighbors(currentBestNode, EnableDiagionalMovement)
			for neighborNode in neighbors:
				if neighborNode in graph.ObstacleNodes:	
					closedNodes.append(neighborNode)
					continue

				
				if (neighborNode in closedNodes):
					continue

				if (neighborNode not in openNodes):
					neighborNode.setParentNode(currentBestNode)
					openNodes.append(neighborNode)
					#continue
				else:
					#print("g(currentBestNode) = " + str(g(currentBestNode)) + " h(currentBestNode,neighborNode) = " + str(h(currentBestNode,neighborNode)) + " g(neighborNode) = " + str(g(neighborNode)))
					if(g(currentBestNode) + h(currentBestNode,neighborNode) < g(neighborNode)):
						neighborNode.setParentNode(currentBestNode)
	return path

#b is backpointer to parent
#c is the length between two nodes


if __name__ == "__main__":
	path = AStar()
	pathString = ""
	#print(path)
	for node in path:
		pathString = pathString + " : " + str(node.getCoords())
		#print(node.getCoords())
	print("path " + pathString)
	
	for node in path:
		if not (node == start or node == end):
			graph.highlightNode(node, "orange")
			
	print(graph.getObstacleCoords())

	graph.drawTiles()
	try:
		if len(path) == 0:
			graph.displayNoSolution()
		graph.getMouse()
	except:
		pass
