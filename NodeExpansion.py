import queue

frontier = queue.deque()

width = 10
height = 10

start = [2,3]
end = [2,8]

all_nodes = []
explored_nodes = []
obstacle_nodes = [[2,5], [3,6],[1,7]]
frontier.append(start)

for x in range(width):
	for y in range(height):
		all_nodes.append([x,y])

def main():
	#print("Node to find neighbor: " + str(all_nodes[1]))
	#print(getNeighbors(all_nodes[10]))
	for StartingNeighbor in getNeighbors(start):
		frontier.append(StartingNeighbor)
	frontier.remove(start)
	explored_nodes.append(start)

	while end not in explored_nodes:
		current = frontier.popleft()
		explored_nodes.append(current)
		for node in getNeighbors(current):
			frontier.append(node)	
		print(frontier)	


	print("Found the end")
		

	

def getNeighbors(node):
	directions = [[-1,0],[0,1],[1,0],[0,-1]] #West, North, East, South

	neighbors = []

	for dir in directions:
		potentialNeighbor = [node[0]+dir[0], node[1]+dir[1]]
		if (potentialNeighbor not in explored_nodes) and (potentialNeighbor not in frontier) and (potentialNeighbor not in obstacle_nodes) and (potentialNeighbor in all_nodes):
			neighbors.append(potentialNeighbor)

	return neighbors



if __name__ == "__main__":
	main()
