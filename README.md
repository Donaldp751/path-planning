Path-planning algorithm demonstration programs

Requires graphics.py installed.

Running:

BFS - python3 Breadth First Search.py
A* - python3 aStar.py
