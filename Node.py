#node class


class Node:
    """A simple node class for path planning programs"""
    x = 0
    y = 0
    weight = 0 #1 is easy to traverse, >10 is impossible

    def __init__(self, x=0, y=0, weight = 0):
        self.x = x
        self.y = y
        self.weight = weight
        self.visited = False

    def setParentNode(self, node):
        self.parent = node

    def getParentNode(self):
        return self.parent

    def setVisited(self):
        self.visited = True

    def isVisited(self):
        return self.visited

    def setWeight(self, weight):
        self.weight = weight

    def getWeight(self):
        return self.weight

    def getX(self):
        return self.x
    
    def getY(self):
        return self.y

    def getCoords(self):
        return [self.x, self.y]

    def __str__(self):
        return str(self.getCoords())

    