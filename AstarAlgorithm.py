import graph
import Node
import math
import json
#import heapq

class AStar:

	def __init__(self, graph, source, destination, enableDiagionalMovement = False, hueristic = 1):
		self.Graph = graph
		self.StartingNode = source
		self.EndingNode = destination
		self.Hueristic = hueristic
		self.EnableDiagionalMovement = enableDiagionalMovement
		self.openNodes = []
		self.closedNodes = []

		self.openNodes.append(self.StartingNode)

	def getNodeCost(self, node, destNode):
		return self.g(node) + self.h(node, destNode)

	def g(self, node):#operating cost function - Actual operating cost having been already traversed
		cost = 1
		traversedNodes = []
		while not node == self.StartingNode:
			try:
				if node in traversedNodes:
					return float("inf")
				traversedNodes.append(node)
				node = node.getParentNode()
				cost = cost+1
			except:
				return cost
			
		return cost

	def h(self, node, destNode): #heuristic function for calculating the distance between the node and the destination node
		#"""Returns the distance between the node""""
		destinationCoords = destNode.getCoords()
		nodeCoords = node.getCoords()
		xDiff = abs(destinationCoords[0]-nodeCoords[0])
		yDiff = abs(destinationCoords[1]-nodeCoords[1])
		return math.sqrt(xDiff*xDiff + yDiff*yDiff) * self.Hueristic

	def getLowestNode(self):
		lowest = float("inf")
		lowestCostingNode = None
		for node in self.openNodes:
			nodeCost = self.getNodeCost(node, self.EndingNode)
			if nodeCost < lowest:
				lowest = nodeCost
				lowestCostingNode = node
		return lowestCostingNode

	def Run(self):
		#graph.getNode(4,3).setParentNode(graph.getNode(3,3))
		#print(g(graph.getNode(4,3)))
		#print(h(graph.getNode(4,3), graph.getNode(8,8)))
		path = []
		while len(self.openNodes):
			currentBestNode = self.getLowestNode()
			self.openNodes.remove(currentBestNode)
			self.closedNodes.append(currentBestNode)

			if currentBestNode == self.EndingNode:
				print("goal met")
				while not currentBestNode == self.StartingNode:
					path.append(currentBestNode)
					try:
						currentBestNode = currentBestNode.getParentNode()
					except:
						path.reverse()
				break
			else:
				neighbors = self.Graph.getNeighbors(currentBestNode, self.EnableDiagionalMovement)
				for neighborNode in neighbors:
					if neighborNode in self.Graph.ObstacleNodes:	
						self.closedNodes.append(neighborNode)
						continue

					
					if (neighborNode in self.closedNodes):
						continue

					if (neighborNode not in self.openNodes):
						neighborNode.setParentNode(currentBestNode)
						self.openNodes.append(neighborNode)
						#continue
					else:
						#print("g(currentBestNode) = " + str(g(currentBestNode)) + " h(currentBestNode,neighborNode) = " + str(h(currentBestNode,neighborNode)) + " g(neighborNode) = " + str(g(neighborNode)))
						if(self.g(currentBestNode) + self.h(currentBestNode,neighborNode) < self.g(neighborNode)):
							neighborNode.setParentNode(currentBestNode)
		return path

if __name__ == "__main__":
	map = graph.Graph(5, 5)

	startLocation = map.getNode(0,0)
	destinationLocation = map.getNode(3,3)

	map.createRandomObstacles(10, [startLocation, destinationLocation])

	obstacleCoords = map.getObstacleCoords()

	jsonEncoder = json.JSONEncoder()

	jsonString = jsonEncoder.encode(obstacleCoords)

	

	print("Json string of obbys")
	print(jsonString)
	

	nav = AStar(map, startLocation, destinationLocation, enableDiagionalMovement=True)

	path = nav.Run()

	for node in nav.Graph.getObstacleCoords():
		print("Obby: " + str(node))
	
	for node in path:
		print(node)
	

